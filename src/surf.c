/* See LICENSE file for copyright and license details.
 *
 * To understand surf, start reading main().
 */
#include <sys/file.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <glib.h>
#include <libgen.h>
#include <limits.h>
#include <pwd.h>
#include <regex.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdkx.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gtk/gtkx.h>
#include <gcr/gcr.h>
#include <JavaScriptCore/JavaScript.h>
#include <webkit2/webkit2.h>
#include <X11/X.h>
#include <X11/Xatom.h>
#include <glib.h>

#define VERSION "1.0"
#define WEBEXTDIR "/usr/local/lib/edere"
#define LENGTH(x)               (sizeof(x) / sizeof(x[0]))
#define CLEANMASK(mask)         (mask & (MODKEY|GDK_SHIFT_MASK))

enum { AtomFind, AtomGo, AtomUri, AtomLast };

enum {
	OnDoc = WEBKIT_HIT_TEST_RESULT_CONTEXT_DOCUMENT,
	OnLink = WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK,
	OnImg = WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE,
	OnMedia = WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA,
	OnEdit = WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE,
	OnBar = WEBKIT_HIT_TEST_RESULT_CONTEXT_SCROLLBAR,
	OnSel = WEBKIT_HIT_TEST_RESULT_CONTEXT_SELECTION,
	OnAny = OnDoc | OnLink | OnImg | OnMedia | OnEdit | OnBar | OnSel,
};

typedef enum {
	AcceleratedCanvas,
	AccessMicrophone,
	AccessWebcam,
	Certificate,
	CookiePolicies,
	DiskCache,
	DefaultCharset,
	DNSPrefetch,
	FileURLsCrossAccess,
	FontSize,
	FrameFlattening,
	Geolocation,
	HideBackground,
	Inspector,
	Java,
	JavaScript,
	KioskMode,
	LoadImages,
	MediaManualPlay,
	Plugins,
	PreferredLanguages,
	RunInFullscreen,
	ScrollBars,
	ShowIndicators,
	SiteQuirks,
	SmoothScrolling,
	SpellChecking,
	SpellLanguages,
	StrictTLS,
	Style,
	WebGL,
	ParameterLast
} ParamName;

typedef union {
	int i;
	float f;
	const void *v;
} Arg;

typedef struct {
	Arg val;
	int prio;
} Parameter;

typedef struct Client {
	GtkWidget *win;
	WebKitWebView *view;
	WebKitWebInspector *inspector;
	WebKitFindController *finder;
	WebKitHitTestResult *mousepos;
	GTlsCertificate *cert, *failedcert;
	GTlsCertificateFlags tlserr;
	Window xid;
	unsigned long pageid;
	int progress, fullscreen, https, insecure, errorpage;
	const char *title, *overtitle, *targeturi;
	const char *needle;
	struct Client *next;
} Client;

typedef struct {
	guint mod;
	guint keyval;
	void (*func)(Client * c, const Arg * a);
	const Arg arg;
} Key;

typedef struct {
	unsigned int target;
	unsigned int mask;
	guint button;
	void (*func)(Client * c, const Arg * a, WebKitHitTestResult * h);
	const Arg arg;
	unsigned int stopevent;
} Button;

typedef struct {
	const char *uri;
	Parameter config[ParameterLast];
	regex_t re;
} UriParameters;

typedef struct {
	char *regex;
	char *file;
	regex_t re;
} SiteSpecific;

/* Surf */
static void sigchld(int unused);
static char *buildfile(const char *path);
static char *buildpath(const char *path);
static char *untildepath(const char *path);
static const char *getuserhomedir(const char *user);
static const char *getcurrentuserhomedir(void);
static Client *newclient(Client * c);
static const char *geturi(Client * c);
static void setatom(Client * c, int a, const char *v);
static const char *getatom(Client * c, int a);
static WebKitCookieAcceptPolicy cookiepolicy_get(void);
static char cookiepolicy_set(const WebKitCookieAcceptPolicy p);
static void seturiparameters(Client * c, const char *uri,
			     ParamName * params);
static void setparameter(Client * c, int refresh, ParamName p,
			 const Arg * a);
static const char *getstyle(const char *uri);
static void setstyle(Client * c, const char *file);
static void updatewinid(Client * c);
static void handleplumb(Client * c, const char *uri);
static void newwindow(Client * c, const Arg * a, int noembed);
static void spawn(Client * c, const Arg * a);
static void msgext(Client * c, char type, const Arg * a);
static void destroyclient(Client * c);
static void cleanup(void);

/* GTK/WebKit */
static WebKitWebView *newview(Client * c, WebKitWebView * rv);
static void initwebextensions(WebKitWebContext * wc, Client * c);
static GtkWidget *createview(WebKitWebView * v, WebKitNavigationAction * a,
			     Client * c);
static gboolean buttonreleased(GtkWidget * w, GdkEvent * e, Client * c);
static gboolean winevent(GtkWidget * w, GdkEvent * e, Client * c);
static gboolean readpipe(GIOChannel * s, GIOCondition ioc,
			 gpointer unused);
static void showview(WebKitWebView * v, Client * c);
static GtkWidget *createwindow(Client * c);
static gboolean loadfailedtls(WebKitWebView * v, gchar * uri,
			      GTlsCertificate * cert,
			      GTlsCertificateFlags err, Client * c);
static void loadchanged(WebKitWebView * v, WebKitLoadEvent e, Client * c);
static void progresschanged(WebKitWebView * v, GParamSpec * ps,
			    Client * c);
static void titlechanged(WebKitWebView * view, GParamSpec * ps,
			 Client * c);
static void mousetargetchanged(WebKitWebView * v, WebKitHitTestResult * h,
			       guint modifiers, Client * c);
static gboolean permissionrequested(WebKitWebView * v,
				    WebKitPermissionRequest * r,
				    Client * c);
static gboolean decidepolicy(WebKitWebView * v, WebKitPolicyDecision * d,
			     WebKitPolicyDecisionType dt, Client * c);
static void decidenavigation(WebKitPolicyDecision * d, Client * c);
static void decideresource(WebKitPolicyDecision * d, Client * c);
static void insecurecontent(WebKitWebView * v,
			    WebKitInsecureContentEvent e, Client * c);
static void webprocessterminated(WebKitWebView * v,
				 WebKitWebProcessTerminationReason r,
				 Client * c);
static void closeview(WebKitWebView * v, Client * c);
static void destroywin(GtkWidget * w, Client * c);

/* Hotkeys */
static void toggle(Client * c, const Arg * a);
static void togglefullscreen(Client * c, const Arg * a);
static void togglecookiepolicy(Client * c, const Arg * a);
static void find(Client * c, const Arg * a);

static char winid[64];
static char togglestats[12];
static char pagestats[2];
static Atom atoms[AtomLast];
static Window embed;
static int showxid;
static int cookiepolicy;
static Display *dpy;
static Client *clients;
static GdkDevice *gdkkb;
static char *stylefile;
static const char *useragent;
static Parameter *curconfig;
static int modparams[ParameterLast];
static int pipein[2], pipeout[2];
char *argv0;

static ParamName loadtransient[] = {
	Certificate,
	CookiePolicies,
	DiskCache,
	DNSPrefetch,
	FileURLsCrossAccess,
	JavaScript,
	LoadImages,
	PreferredLanguages,
	ShowIndicators,
	StrictTLS,
	ParameterLast
};

static ParamName loadcommitted[] = {
	AcceleratedCanvas,
	DefaultCharset,
	FontSize,
	FrameFlattening,
	Geolocation,
	HideBackground,
	Inspector,
	Java,
	MediaManualPlay,
	Plugins,
	RunInFullscreen,
	ScrollBars,
	SiteQuirks,
	SmoothScrolling,
	SpellChecking,
	SpellLanguages,
	Style,
	ParameterLast
};

static ParamName loadfinished[] = {
	ParameterLast
};

/* configuration, allows nested code to access above variables */
#include "config.h"
#define MSGBUFSZ 256


void setup()
{
	GIOChannel *gchanin;
	int i, j;

	dpy = XOpenDisplay(NULL);
	gtk_init(NULL, NULL);

	GdkDisplay *gdpy = gdk_display_get_default();
	curconfig = defconfig;

	/* dirs and files */
	cookiefile = buildfile(cookiefile);
	cachedir = buildpath(cachedir);

	gdkkb = gdk_seat_get_keyboard(gdk_display_get_default_seat(gdpy));

	if (pipe(pipeout) < 0 || pipe(pipein) < 0) {
		fputs("Unable to create pipes\n", stderr);
	} else {
		gchanin = g_io_channel_unix_new(pipein[0]);
		g_io_channel_set_encoding(gchanin, NULL, NULL);
		g_io_channel_set_close_on_unref(gchanin, TRUE);
		g_io_add_watch(gchanin, G_IO_IN, readpipe, NULL);
	}
}

void sigchld(int unused)
{
	if (signal(SIGCHLD, sigchld) == SIG_ERR) {
		printf("Can't install SIGCHLD handler\n");
		exit(EXIT_FAILURE);
	}
	while (waitpid(-1, NULL, WNOHANG) > 0);
}

char *buildfile(const char *path)
{
	char *dname, *bname, *bpath, *fpath;

	/* build path */
	dname = g_path_get_dirname(path);
	bpath = buildpath(dname);
	g_free(dname);

	/* build file */
	bname = g_path_get_basename(path);
	fpath = g_build_filename(bpath, bname, NULL);
	g_free(bpath);
	g_free(bname);

	/* Open file */
	FILE *f = fopen(fpath, "a");
	if (f == NULL) {
		printf("Could not open file: %s\n", fpath);
		exit(EXIT_FAILURE);
	}

	g_chmod(fpath, 0600);	/* always */
	fclose(f);
	return fpath;
}

static const char *getuserhomedir(const char *user)
{
	struct passwd *pw = getpwnam(user);

	if (!pw) {
		printf("Can't get user %s login information.\n", user);
		exit(EXIT_FAILURE);
	}

	return pw->pw_dir;
}

static const char *getcurrentuserhomedir(void)
{
	const char *homedir;
	const char *user;
	struct passwd *pw;

	homedir = getenv("HOME");
	if (homedir)
		return homedir;

	user = getenv("USER");
	if (user)
		return getuserhomedir(user);

	pw = getpwuid(getuid());
	if (!pw) {
		printf("Can't get current user home directory\n");
		exit(EXIT_FAILURE);
	}

	return pw->pw_dir;
}

char *buildpath(const char *path)
{
	char *apath, *fpath;

	if (path[0] == '~')
		apath = untildepath(path);
	else
		apath = g_strdup(path);

	/* creating directory */
	if (g_mkdir_with_parents(apath, 0700) < 0) {
		printf("Could not access directory: %s\n", apath);
		exit(EXIT_FAILURE);
	}

	fpath = realpath(apath, NULL);
	g_free(apath);

	return fpath;
}

char *untildepath(const char *path)
{
	char *apath, *name, *p;
	const char *homedir;

	if (path[1] == '/' || path[1] == '\0') {
		p = (char *) &path[1];
		homedir = getcurrentuserhomedir();
	} else {
		if ((p = strchr(path, '/')))
			name = g_strndup(&path[1], p - (path + 1));
		else
			name = g_strdup(&path[1]);

		homedir = getuserhomedir(name);
		g_free(name);
	}
	apath = g_build_filename(homedir, p, NULL);
	return apath;
}

Client *newclient(Client * rc)
{
	Client *c = malloc(sizeof(Client));

	c->next = clients;
	clients = c;

	c->progress = 100;
	c->view = newview(c, rc ? rc->view : NULL);

	return c;
}

const char *geturi(Client * c)
{
	const char *uri;

	if (!(uri = webkit_web_view_get_uri(c->view)))
		uri = "about:blank";
	return uri;
}

void setatom(Client * c, int a, const char *v)
{
	XChangeProperty(dpy, c->xid,
			atoms[a], XA_STRING, 8, PropModeReplace,
			(unsigned char *) v, strlen(v) + 1);
	XSync(dpy, False);
}

const char *getatom(Client * c, int a)
{
	static char buf[BUFSIZ];
	Atom adummy;
	int idummy;
	unsigned long ldummy;
	unsigned char *p = NULL;

	XSync(dpy, False);
	XGetWindowProperty(dpy, c->xid, atoms[a], 0L, BUFSIZ, False,
			   XA_STRING, &adummy, &idummy, &ldummy, &ldummy,
			   &p);
	if (p)
		strncpy(buf, (char *) p, LENGTH(buf) - 1);
	else
		buf[0] = '\0';
	XFree(p);

	return buf;
}

WebKitCookieAcceptPolicy cookiepolicy_get(void)
{
	switch (((char *) curconfig[CookiePolicies].val.v)[cookiepolicy]) {
	case 'a':
		return WEBKIT_COOKIE_POLICY_ACCEPT_NEVER;
	case '@':
		return WEBKIT_COOKIE_POLICY_ACCEPT_NO_THIRD_PARTY;
	default:		/* fallthrough */
	case 'A':
		return WEBKIT_COOKIE_POLICY_ACCEPT_ALWAYS;
	}
}

char cookiepolicy_set(const WebKitCookieAcceptPolicy p)
{
	switch (p) {
	case WEBKIT_COOKIE_POLICY_ACCEPT_NEVER:
		return 'a';
	case WEBKIT_COOKIE_POLICY_ACCEPT_NO_THIRD_PARTY:
		return '@';
	default:		/* fallthrough */
	case WEBKIT_COOKIE_POLICY_ACCEPT_ALWAYS:
		return 'A';
	}
}

void seturiparameters(Client * c, const char *uri, ParamName * params)
{
	Parameter *config, *uriconfig = NULL;
	int i, p;

	curconfig = uriconfig ? uriconfig : defconfig;

	for (i = 0; (p = params[i]) != ParameterLast; ++i) {
		switch (p) {
		default:	/* FALLTHROUGH */
			if (!(defconfig[p].prio < curconfig[p].prio ||
			      defconfig[p].prio < modparams[p]))
				continue;
		case Certificate:
		case CookiePolicies:
		case Style:
			setparameter(c, 0, p, &curconfig[p].val);
		}
	}
}

void setparameter(Client * c, int refresh, ParamName p, const Arg * a)
{
	GdkRGBA bgcolor = { 0 };
	WebKitSettings *s = webkit_web_view_get_settings(c->view);

	modparams[p] = curconfig[p].prio;

	switch (p) {
	case AcceleratedCanvas:
		webkit_settings_set_enable_accelerated_2d_canvas(s, a->i);
		break;
	case AccessMicrophone:
		return;		/* do nothing */
	case AccessWebcam:
		return;		/* do nothing */
	case CookiePolicies:
		webkit_cookie_manager_set_accept_policy
		    (webkit_web_context_get_cookie_manager
		     (webkit_web_view_get_context(c->view)),
		     cookiepolicy_get());
		refresh = 0;
		break;
	case DiskCache:
		webkit_web_context_set_cache_model
		    (webkit_web_view_get_context(c->view),
		     a->
		     i ? WEBKIT_CACHE_MODEL_WEB_BROWSER :
		     WEBKIT_CACHE_MODEL_DOCUMENT_VIEWER);
		return;		/* do not update */
	case DefaultCharset:
		webkit_settings_set_default_charset(s, a->v);
		return;		/* do not update */
	case DNSPrefetch:
		webkit_settings_set_enable_dns_prefetching(s, a->i);
		return;		/* do not update */
	case FileURLsCrossAccess:
		webkit_settings_set_allow_file_access_from_file_urls(s,
								     a->i);
		webkit_settings_set_allow_universal_access_from_file_urls
		    (s, a->i);
		return;		/* do not update */
	case FontSize:
		webkit_settings_set_default_font_size(s, a->i);
		return;		/* do not update */
	case FrameFlattening:
		webkit_settings_set_enable_frame_flattening(s, a->i);
		break;
	case Geolocation:
		refresh = 0;
		break;
	case HideBackground:
		if (a->i)
			webkit_web_view_set_background_color(c->view,
							     &bgcolor);
		return;		/* do not update */
	case Inspector:
		webkit_settings_set_enable_developer_extras(s, a->i);
		return;		/* do not update */
	case Java:
		webkit_settings_set_enable_java(s, a->i);
		return;		/* do not update */
	case JavaScript:
		webkit_settings_set_enable_javascript(s, a->i);
		break;
	case KioskMode:
		return;		/* do nothing */
	case LoadImages:
		webkit_settings_set_auto_load_images(s, a->i);
		break;
	case MediaManualPlay:
		webkit_settings_set_media_playback_requires_user_gesture(s,
									 a->
									 i);
		break;
	case Plugins:
		webkit_settings_set_enable_plugins(s, a->i);
		break;
	case PreferredLanguages:
		return;		/* do nothing */
	case RunInFullscreen:
		return;		/* do nothing */
	case ShowIndicators:
		break;
	case SmoothScrolling:
		webkit_settings_set_enable_smooth_scrolling(s, a->i);
		return;		/* do not update */
	case SiteQuirks:
		webkit_settings_set_enable_site_specific_quirks(s, a->i);
		break;
	case SpellChecking:
		webkit_web_context_set_spell_checking_enabled
		    (webkit_web_view_get_context(c->view), a->i);
		return;		/* do not update */
	case SpellLanguages:
		return;		/* do nothing */
	case StrictTLS:
		webkit_web_context_set_tls_errors_policy
		    (webkit_web_view_get_context(c->view),
		     a->
		     i ? WEBKIT_TLS_ERRORS_POLICY_FAIL :
		     WEBKIT_TLS_ERRORS_POLICY_IGNORE);
		break;
	case Style:
		webkit_user_content_manager_remove_all_style_sheets
		    (webkit_web_view_get_user_content_manager(c->view));
		if (a->i)
			setstyle(c, getstyle(geturi(c)));
		refresh = 0;
		break;
	case WebGL:
		webkit_settings_set_enable_webgl(s, a->i);
		break;
	default:
		return;		/* do nothing */
	}
}

const char *getstyle(const char *uri)
{
	int i;

	if (stylefile)
		return stylefile;

	return "";
}

void setstyle(Client * c, const char *file)
{
	gchar *style;

	if (!g_file_get_contents(file, &style, NULL, NULL)) {
		fprintf(stderr, "Could not read style file: %s\n", file);
		return;
	}

	webkit_user_content_manager_add_style_sheet
	    (webkit_web_view_get_user_content_manager(c->view),
	     webkit_user_style_sheet_new(style,
					 WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
					 WEBKIT_USER_STYLE_LEVEL_USER,
					 NULL, NULL));

	g_free(style);
}

void updatewinid(Client * c)
{
	snprintf(winid, LENGTH(winid), "%lu", c->xid);
}

void handleplumb(Client * c, const char *uri)
{
	Arg a = (Arg) PLUMB(uri);
	spawn(c, &a);
}

void newwindow(Client * c, const Arg * a, int noembed)
{
	int i = 0;
	char tmp[64];
	const char *cmd[29], *uri;
	const Arg arg = {.v = cmd };

	cmd[i++] = argv0;
	cmd[i++] = "-a";
	cmd[i++] = curconfig[CookiePolicies].val.v;
	cmd[i++] = curconfig[ScrollBars].val.i ? "-B" : "-b";
	if (cookiefile && g_strcmp0(cookiefile, "")) {
		cmd[i++] = "-c";
		cmd[i++] = cookiefile;
	}
	if (stylefile && g_strcmp0(stylefile, "")) {
		cmd[i++] = "-C";
		cmd[i++] = stylefile;
	}
	cmd[i++] = curconfig[DiskCache].val.i ? "-D" : "-d";
	if (embed && !noembed) {
		cmd[i++] = "-e";
		snprintf(tmp, LENGTH(tmp), "%lu", embed);
		cmd[i++] = tmp;
	}
	cmd[i++] = curconfig[RunInFullscreen].val.i ? "-F" : "-f";
	cmd[i++] = curconfig[Geolocation].val.i ? "-G" : "-g";
	cmd[i++] = curconfig[LoadImages].val.i ? "-I" : "-i";
	cmd[i++] = curconfig[KioskMode].val.i ? "-K" : "-k";
	cmd[i++] = curconfig[Style].val.i ? "-M" : "-m";
	cmd[i++] = curconfig[Inspector].val.i ? "-N" : "-n";
	cmd[i++] = curconfig[Plugins].val.i ? "-P" : "-p";
	cmd[i++] = curconfig[JavaScript].val.i ? "-S" : "-s";
	cmd[i++] = curconfig[StrictTLS].val.i ? "-T" : "-t";
	if (fulluseragent && g_strcmp0(fulluseragent, "")) {
		cmd[i++] = "-u";
		cmd[i++] = fulluseragent;
	}
	if (showxid)
		cmd[i++] = "-w";
	cmd[i++] = curconfig[Certificate].val.i ? "-X" : "-x";
	/* do not keep zoom level */
	cmd[i++] = "--";
	if ((uri = a->v))
		cmd[i++] = uri;
	cmd[i] = NULL;

	spawn(c, &arg);
}

void spawn(Client * c, const Arg * a)
{
	if (fork() == 0) {
		if (dpy)
			close(ConnectionNumber(dpy));
		close(pipein[0]);
		close(pipeout[1]);
		setsid();
		execvp(((char **) a->v)[0], (char **) a->v);
		fprintf(stderr, "%s: execvp %s", argv0,
			((char **) a->v)[0]);
		perror(" failed");
		exit(1);
	}
}

void destroyclient(Client * c)
{
	Client *p;

	webkit_web_view_stop_loading(c->view);
	/* Not needed, has already been called
	   gtk_widget_destroy(c->win);
	 */

	for (p = clients; p && p->next != c; p = p->next);
	if (p)
		p->next = c->next;
	else
		clients = c->next;
	free(c);
}

void cleanup(void)
{
	while (clients)
		destroyclient(clients);

	close(pipein[0]);
	close(pipeout[1]);
	g_free(cookiefile);
	g_free(stylefile);
	g_free(cachedir);
	XCloseDisplay(dpy);
}

WebKitWebView *newview(Client * c, WebKitWebView * rv)
{
	WebKitWebView *v;
	WebKitSettings *settings;
	WebKitWebContext *context;
	WebKitCookieManager *cookiemanager;
	WebKitUserContentManager *contentmanager;

	/* Webview */
	if (rv) {
		v = WEBKIT_WEB_VIEW(webkit_web_view_new_with_related_view
				    (rv));
	} else {
		settings =
		    webkit_settings_new_with_settings
		    ("allow-file-access-from-file-urls",
		     curconfig[FileURLsCrossAccess].val.i,
		     "allow-universal-access-from-file-urls",
		     curconfig[FileURLsCrossAccess].val.i,
		     "auto-load-images", curconfig[LoadImages].val.i,
		     "default-charset", curconfig[DefaultCharset].val.v,
		     "default-font-size", curconfig[FontSize].val.i,
		     "enable-caret-browsing", 0, "enable-developer-extras",
		     curconfig[Inspector].val.i, "enable-dns-prefetching",
		     curconfig[DNSPrefetch].val.i,
		     "enable-frame-flattening",
		     curconfig[FrameFlattening].val.i,
		     "enable-html5-database", curconfig[DiskCache].val.i,
		     "enable-html5-local-storage",
		     curconfig[DiskCache].val.i, "enable-java",
		     curconfig[Java].val.i, "enable-javascript",
		     curconfig[JavaScript].val.i, "enable-plugins",
		     curconfig[Plugins].val.i,
		     "enable-accelerated-2d-canvas",
		     curconfig[AcceleratedCanvas].val.i,
		     "enable-site-specific-quirks",
		     curconfig[SiteQuirks].val.i,
		     "enable-smooth-scrolling",
		     curconfig[SmoothScrolling].val.i, "enable-webgl",
		     curconfig[WebGL].val.i,
		     "media-playback-requires-user-gesture",
		     curconfig[MediaManualPlay].val.i, NULL);
/* For more interesting settings, have a look at
 * http://webkitgtk.org/reference/webkit2gtk/stable/WebKitSettings.html */

		if (strcmp(fulluseragent, "")) {
			webkit_settings_set_user_agent(settings,
						       fulluseragent);
		} else if (surfuseragent) {
			webkit_settings_set_user_agent_with_application_details
			    (settings, "Surf", VERSION);
		}
		useragent = webkit_settings_get_user_agent(settings);

		contentmanager = webkit_user_content_manager_new();

		context =
		    webkit_web_context_new_with_website_data_manager
		    (webkit_website_data_manager_new
		     ("base-cache-directory", cachedir,
		      "base-data-directory", cachedir, NULL));

		cookiemanager =
		    webkit_web_context_get_cookie_manager(context);

		/* rendering process model, can be a shared unique one
		 * or one for each view */
		webkit_web_context_set_process_model(context,
						     WEBKIT_PROCESS_MODEL_MULTIPLE_SECONDARY_PROCESSES);
		/* TLS */
		webkit_web_context_set_tls_errors_policy(context,
							 curconfig
							 [StrictTLS].val.
							 i ?
							 WEBKIT_TLS_ERRORS_POLICY_FAIL
							 :
							 WEBKIT_TLS_ERRORS_POLICY_IGNORE);
		/* disk cache */
		webkit_web_context_set_cache_model(context,
						   curconfig[DiskCache].
						   val.
						   i ?
						   WEBKIT_CACHE_MODEL_WEB_BROWSER
						   :
						   WEBKIT_CACHE_MODEL_DOCUMENT_VIEWER);

		/* Currently only works with text file to be compatible with curl */
		webkit_cookie_manager_set_persistent_storage(cookiemanager,
							     cookiefile,
							     WEBKIT_COOKIE_PERSISTENT_STORAGE_TEXT);
		/* cookie policy */
		webkit_cookie_manager_set_accept_policy(cookiemanager,
							cookiepolicy_get
							());
		/* languages */
		webkit_web_context_set_preferred_languages(context,
							   curconfig
							   [PreferredLanguages].
							   val.v);
		webkit_web_context_set_spell_checking_languages(context,
								curconfig
								[SpellLanguages].
								val.v);
		webkit_web_context_set_spell_checking_enabled(context,
							      curconfig
							      [SpellChecking].
							      val.i);

		g_signal_connect(G_OBJECT(context),
				 "initialize-web-extensions",
				 G_CALLBACK(initwebextensions), c);

		v = g_object_new(WEBKIT_TYPE_WEB_VIEW,
				 "settings", settings,
				 "user-content-manager", contentmanager,
				 "web-context", context, NULL);
	}

	g_signal_connect(G_OBJECT(v), "notify::estimated-load-progress",
			 G_CALLBACK(progresschanged), c);
	g_signal_connect(G_OBJECT(v), "notify::title",
			 G_CALLBACK(titlechanged), c);
	g_signal_connect(G_OBJECT(v), "button-release-event",
			 G_CALLBACK(buttonreleased), c);
	g_signal_connect(G_OBJECT(v), "close", G_CALLBACK(closeview), c);
	g_signal_connect(G_OBJECT(v), "create", G_CALLBACK(createview), c);
	g_signal_connect(G_OBJECT(v), "decide-policy",
			 G_CALLBACK(decidepolicy), c);
	g_signal_connect(G_OBJECT(v), "insecure-content-detected",
			 G_CALLBACK(insecurecontent), c);
	g_signal_connect(G_OBJECT(v), "load-failed-with-tls-errors",
			 G_CALLBACK(loadfailedtls), c);
	g_signal_connect(G_OBJECT(v), "load-changed",
			 G_CALLBACK(loadchanged), c);
	g_signal_connect(G_OBJECT(v), "mouse-target-changed",
			 G_CALLBACK(mousetargetchanged), c);
	g_signal_connect(G_OBJECT(v), "permission-request",
			 G_CALLBACK(permissionrequested), c);
	g_signal_connect(G_OBJECT(v), "ready-to-show",
			 G_CALLBACK(showview), c);
	g_signal_connect(G_OBJECT(v), "web-process-terminated",
			 G_CALLBACK(webprocessterminated), c);

	return v;
}

static gboolean readpipe(GIOChannel * s, GIOCondition ioc, gpointer unused)
{
	static char msg[MSGBUFSZ], msgsz;
	GError *gerr = NULL;

	if (g_io_channel_read_chars(s, msg, sizeof(msg), NULL, &gerr) !=
	    G_IO_STATUS_NORMAL) {
		fprintf(stderr, "surf: error reading pipe: %s\n",
			gerr->message);
		g_error_free(gerr);
		return TRUE;
	}
	if ((msgsz = msg[0]) < 3) {
		fprintf(stderr, "surf: message too short: %d\n", msgsz);
		return TRUE;
	}

	switch (msg[2]) {
	case 'i':
		close(pipein[1]);
		close(pipeout[0]);
		break;
	}

	return TRUE;
}

void initwebextensions(WebKitWebContext * wc, Client * c)
{
	GVariant *gv;

	if (!pipeout[0] || !pipein[1])
		return;

	gv = g_variant_new("(ii)", pipeout[0], pipein[1]);

	webkit_web_context_set_web_extensions_initialization_user_data(wc,
								       gv);
	webkit_web_context_set_web_extensions_directory(wc, WEBEXTDIR);
}

GtkWidget *createview(WebKitWebView * v, WebKitNavigationAction * a,
		      Client * c)
{
	Client *n;

	switch (webkit_navigation_action_get_navigation_type(a)) {
	case WEBKIT_NAVIGATION_TYPE_OTHER:	/* fallthrough */
		/*
		 * popup windows of type “other” are almost always triggered
		 * by user gesture, so inverse the logic here
		 */
/* instead of this, compare destination uri to mouse-over uri for validating window */
		if (webkit_navigation_action_is_user_gesture(a))
			return NULL;

	default:
		return NULL;
	}

	return GTK_WIDGET(n->view);
}

gboolean buttonreleased(GtkWidget * w, GdkEvent * e, Client * c)
{
	WebKitHitTestResultContext element;
	int i;

	element = webkit_hit_test_result_get_context(c->mousepos);
	return FALSE;
}

gboolean winevent(GtkWidget * w, GdkEvent * e, Client * c)
{
	int i;

	switch (e->type) {
	case GDK_ENTER_NOTIFY:
		c->overtitle = c->targeturi;
		break;
	case GDK_KEY_PRESS:
		if (!curconfig[KioskMode].val.i) {
			for (i = 0; i < LENGTH(keys); ++i) {
				if (gdk_keyval_to_lower(e->key.keyval) ==
				    keys[i].keyval &&
				    CLEANMASK(e->key.state) == keys[i].mod
				    && keys[i].func) {
					updatewinid(c);
					keys[i].func(c, &(keys[i].arg));
					return TRUE;
				}
			}
		}
	case GDK_LEAVE_NOTIFY:
		c->overtitle = NULL;
		break;
	case GDK_WINDOW_STATE:
		if (e->window_state.changed_mask ==
		    GDK_WINDOW_STATE_FULLSCREEN)
			c->fullscreen = e->window_state.new_window_state &
			    GDK_WINDOW_STATE_FULLSCREEN;
		break;
	default:
		break;
	}

	return FALSE;
}

void showview(WebKitWebView * v, Client * c)
{
	GdkRGBA bgcolor = { 0 };
	GdkWindow *gwin;

	c->finder = webkit_web_view_get_find_controller(c->view);
	c->inspector = webkit_web_view_get_inspector(c->view);

	c->pageid = webkit_web_view_get_page_id(c->view);
	c->win = createwindow(c);

	gtk_container_add(GTK_CONTAINER(c->win), GTK_WIDGET(c->view));
	gtk_widget_show_all(c->win);
	gtk_widget_grab_focus(GTK_WIDGET(c->view));

	gwin = gtk_widget_get_window(GTK_WIDGET(c->win));
	c->xid = gdk_x11_window_get_xid(gwin);
	updatewinid(c);
	if (showxid) {
		gdk_display_sync(gtk_widget_get_display(c->win));
		puts(winid);
		fflush(stdout);
	}

	if (curconfig[HideBackground].val.i)
		webkit_web_view_set_background_color(c->view, &bgcolor);

	if (curconfig[RunInFullscreen].val.i)
		togglefullscreen(c, NULL);

	setatom(c, AtomFind, "");
	setatom(c, AtomUri, "about:blank");
}

GtkWidget *createwindow(Client * c)
{
	char *wmstr;
	GtkWidget *w;

	if (embed) {
		w = gtk_plug_new(embed);
	} else {
		w = gtk_window_new(GTK_WINDOW_TOPLEVEL);

		wmstr = g_path_get_basename(argv0);
		gtk_window_set_wmclass(GTK_WINDOW(w), wmstr, "Surf");
		g_free(wmstr);

		wmstr = g_strdup_printf("%s[%lu]", "Surf", c->pageid);
		gtk_window_set_role(GTK_WINDOW(w), wmstr);
		g_free(wmstr);

		gtk_window_set_default_size(GTK_WINDOW(w), winsize[0],
					    winsize[1]);
	}

	g_signal_connect(G_OBJECT(w), "destroy",
			 G_CALLBACK(destroywin), c);
	g_signal_connect(G_OBJECT(w), "enter-notify-event",
			 G_CALLBACK(winevent), c);
	g_signal_connect(G_OBJECT(w), "key-press-event",
			 G_CALLBACK(winevent), c);
	g_signal_connect(G_OBJECT(w), "leave-notify-event",
			 G_CALLBACK(winevent), c);
	g_signal_connect(G_OBJECT(w), "window-state-event",
			 G_CALLBACK(winevent), c);

	return w;
}

gboolean
loadfailedtls(WebKitWebView * v, gchar * uri, GTlsCertificate * cert,
	      GTlsCertificateFlags err, Client * c)
{
	GString *errmsg = g_string_new(NULL);
	gchar *html, *pem;

	c->failedcert = g_object_ref(cert);
	c->tlserr = err;
	c->errorpage = 1;

	if (err & G_TLS_CERTIFICATE_UNKNOWN_CA)
		g_string_append(errmsg,
				"The signing certificate authority is not known.<br>");
	if (err & G_TLS_CERTIFICATE_BAD_IDENTITY)
		g_string_append(errmsg,
				"The certificate does not match the expected identity "
				"of the site that it was retrieved from.<br>");
	if (err & G_TLS_CERTIFICATE_NOT_ACTIVATED)
		g_string_append(errmsg,
				"The certificate's activation time "
				"is still in the future.<br>");
	if (err & G_TLS_CERTIFICATE_EXPIRED)
		g_string_append(errmsg,
				"The certificate has expired.<br>");
	if (err & G_TLS_CERTIFICATE_REVOKED)
		g_string_append(errmsg,
				"The certificate has been revoked according to "
				"the GTlsConnection's certificate revocation list.<br>");
	if (err & G_TLS_CERTIFICATE_INSECURE)
		g_string_append(errmsg,
				"The certificate's algorithm is considered insecure.<br>");
	if (err & G_TLS_CERTIFICATE_GENERIC_ERROR)
		g_string_append(errmsg,
				"Some error occurred validating the certificate.<br>");

	g_object_get(cert, "certificate-pem", &pem, NULL);
	html =
	    g_strdup_printf
	    ("<p>Could not validate TLS for “%s”<br>%s</p>"
	     "<p>You can inspect the following certificate "
	     "with Ctrl-t (default keybinding).</p>"
	     "<p><pre>%s</pre></p>", uri, errmsg->str, pem);
	g_free(pem);
	g_string_free(errmsg, TRUE);

	webkit_web_view_load_alternate_html(c->view, html, uri, NULL);
	g_free(html);

	return TRUE;
}

void loadchanged(WebKitWebView * v, WebKitLoadEvent e, Client * c)
{
	const char *uri = geturi(c);

	switch (e) {
	case WEBKIT_LOAD_STARTED:
		setatom(c, AtomUri, uri);
		c->title = uri;
		c->https = c->insecure = 0;
		seturiparameters(c, uri, loadtransient);
		if (c->errorpage)
			c->errorpage = 0;
		else
			g_clear_object(&c->failedcert);
		break;
	case WEBKIT_LOAD_REDIRECTED:
		setatom(c, AtomUri, uri);
		c->title = uri;
		seturiparameters(c, uri, loadtransient);
		break;
	case WEBKIT_LOAD_COMMITTED:
		seturiparameters(c, uri, loadcommitted);
		c->https = webkit_web_view_get_tls_info(c->view, &c->cert,
							&c->tlserr);
		break;
	case WEBKIT_LOAD_FINISHED:
		seturiparameters(c, uri, loadfinished);
		break;
	}
}

void progresschanged(WebKitWebView * v, GParamSpec * ps, Client * c)
{
	c->progress =
	    webkit_web_view_get_estimated_load_progress(c->view) * 100;
}

void titlechanged(WebKitWebView * view, GParamSpec * ps, Client * c)
{
	c->title = webkit_web_view_get_title(c->view);
}

void
mousetargetchanged(WebKitWebView * v, WebKitHitTestResult * h,
		   guint modifiers, Client * c)
{
	WebKitHitTestResultContext hc =
	    webkit_hit_test_result_get_context(h);

	/* Keep the hit test to know where is the pointer on the next click */
	c->mousepos = h;

	if (hc & OnLink)
		c->targeturi = webkit_hit_test_result_get_link_uri(h);
	else if (hc & OnImg)
		c->targeturi = webkit_hit_test_result_get_image_uri(h);
	else if (hc & OnMedia)
		c->targeturi = webkit_hit_test_result_get_media_uri(h);
	else
		c->targeturi = NULL;

	c->overtitle = c->targeturi;
}

gboolean
permissionrequested(WebKitWebView * v, WebKitPermissionRequest * r,
		    Client * c)
{
	ParamName param = ParameterLast;

	if (WEBKIT_IS_GEOLOCATION_PERMISSION_REQUEST(r)) {
		param = Geolocation;
	} else if (WEBKIT_IS_USER_MEDIA_PERMISSION_REQUEST(r)) {
		if (webkit_user_media_permission_is_for_audio_device
		    (WEBKIT_USER_MEDIA_PERMISSION_REQUEST(r)))
			param = AccessMicrophone;
		else if (webkit_user_media_permission_is_for_video_device
			 (WEBKIT_USER_MEDIA_PERMISSION_REQUEST(r)))
			param = AccessWebcam;
	} else {
		return FALSE;
	}

	if (curconfig[param].val.i)
		webkit_permission_request_allow(r);
	else
		webkit_permission_request_deny(r);

	return TRUE;
}

gboolean
decidepolicy(WebKitWebView * v, WebKitPolicyDecision * d,
	     WebKitPolicyDecisionType dt, Client * c)
{
	switch (dt) {
	case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
		decidenavigation(d, c);
		break;
	case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
		decideresource(d, c);
		break;
	default:
		webkit_policy_decision_ignore(d);
		break;
	}
	return TRUE;
}

void decidenavigation(WebKitPolicyDecision * d, Client * c)
{
	WebKitNavigationAction *a =
	    webkit_navigation_policy_decision_get_navigation_action
	    (WEBKIT_NAVIGATION_POLICY_DECISION(d));

	switch (webkit_navigation_action_get_navigation_type(a)) {
	case WEBKIT_NAVIGATION_TYPE_LINK_CLICKED:	/* fallthrough */
	case WEBKIT_NAVIGATION_TYPE_FORM_SUBMITTED:	/* fallthrough */
	case WEBKIT_NAVIGATION_TYPE_BACK_FORWARD:	/* fallthrough */
	case WEBKIT_NAVIGATION_TYPE_RELOAD:	/* fallthrough */
	case WEBKIT_NAVIGATION_TYPE_FORM_RESUBMITTED:	/* fallthrough */
	case WEBKIT_NAVIGATION_TYPE_OTHER:	/* fallthrough */
	default:
		/* Do not navigate to links with a "_blank" target (popup) */
		if (webkit_navigation_policy_decision_get_frame_name
		    (WEBKIT_NAVIGATION_POLICY_DECISION(d))) {
			webkit_policy_decision_ignore(d);
		} else {
			/* Filter out navigation to different domain ? */
			/* get action→urirequest, copy and load in new window+view
			 * on Ctrl+Click ? */
			webkit_policy_decision_use(d);
		}
		break;
	}
}

void decideresource(WebKitPolicyDecision * d, Client * c)
{
	int i, isascii = 1;
	WebKitResponsePolicyDecision *r =
	    WEBKIT_RESPONSE_POLICY_DECISION(d);
	WebKitURIResponse *res =
	    webkit_response_policy_decision_get_response(r);
	const gchar *uri = webkit_uri_response_get_uri(res);

	if (g_str_has_suffix(uri, "/favicon.ico")) {
		webkit_policy_decision_ignore(d);
		return;
	}

	if (!g_str_has_prefix(uri, "http://")
	    && !g_str_has_prefix(uri, "https://")
	    && !g_str_has_prefix(uri, "about:")
	    && !g_str_has_prefix(uri, "file://")
	    && !g_str_has_prefix(uri, "data:")
	    && !g_str_has_prefix(uri, "blob:")
	    && strlen(uri) > 0) {
		for (i = 0; i < strlen(uri); i++) {
			if (!g_ascii_isprint(uri[i])) {
				isascii = 0;
				break;
			}
		}
		if (isascii) {
			handleplumb(c, uri);
			webkit_policy_decision_ignore(d);
			return;
		}
	}

	if (webkit_response_policy_decision_is_mime_type_supported(r))
		webkit_policy_decision_use(d);
	else
		webkit_policy_decision_ignore(d);
}

void insecurecontent(WebKitWebView * v, WebKitInsecureContentEvent e,
		Client * c)
{
	c->insecure = 1;
}

void webprocessterminated(WebKitWebView * v,
		     WebKitWebProcessTerminationReason r, Client * c)
{
	fprintf(stderr, "web process terminated: %s\n",
		r == WEBKIT_WEB_PROCESS_CRASHED ? "crashed" : "no memory");
	closeview(v, c);
}

void closeview(WebKitWebView * v, Client * c)
{
	gtk_widget_destroy(c->win);
}

void destroywin(GtkWidget * w, Client * c)
{
	destroyclient(c);
	if (!clients)
		gtk_main_quit();
}

static void msgext(Client * c, char type, const Arg * a)
{
	static char msg[MSGBUFSZ];
	int ret;

	if ((ret = snprintf(msg, sizeof(msg), "%c%c%c%c",
			    4, c->pageid, type, a->i))
	    >= sizeof(msg)) {
		fprintf(stderr, "surf: message too long: %d\n", ret);
		return;
	}

	if (pipeout[1] && write(pipeout[1], msg, sizeof(msg)) < 0)
		fprintf(stderr, "surf: error sending: %.*s\n", ret - 2,
			msg + 2);
}

void toggle(Client * c, const Arg * a)
{
	curconfig[a->i].val.i ^= 1;
	setparameter(c, 1, (ParamName) a->i, &curconfig[a->i].val);
}

void togglefullscreen(Client * c, const Arg * a)
{
	/* toggling value is handled in winevent() */
	if (c->fullscreen)
		gtk_window_unfullscreen(GTK_WINDOW(c->win));
	else
		gtk_window_fullscreen(GTK_WINDOW(c->win));
}

void togglecookiepolicy(Client * c, const Arg * a)
{
	++cookiepolicy;
	cookiepolicy %= strlen(curconfig[CookiePolicies].val.v);

	setparameter(c, 0, CookiePolicies, NULL);
}

void find(Client * c, const Arg * a)
{
	const char *s, *f;

	if (a && a->i) {
		if (a->i > 0)
			webkit_find_controller_search_next(c->finder);
		else
			webkit_find_controller_search_previous(c->finder);
	} else {
		s = getatom(c, AtomFind);
		f = webkit_find_controller_get_search_text(c->finder);

		if (g_strcmp0(f, s) == 0)	/* reset search */
			webkit_find_controller_search(c->finder, "",
						      findopts, G_MAXUINT);

		webkit_find_controller_search(c->finder, s, findopts,
					      G_MAXUINT);

		if (strcmp(s, "") == 0)
			webkit_find_controller_search_finish(c->finder);
	}
}

void setview(Client * c)
{
	gtk_window_set_title(GTK_WINDOW(c->win), "Edere Editor");
	webkit_web_view_load_uri(c->view,
				 "https://repl.it/languages/html");
}

int main(int argc, char *argv[])
{
	setup();

	Client *c = newclient(NULL);
	showview(NULL, c);
	setview(c);

	gtk_main();
	cleanup();
}
